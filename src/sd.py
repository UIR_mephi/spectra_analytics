# -*- coding: utf-8 -*-
import logging
from obj_common import *

logger = logging.getLogger('sd')


@register
class SolutionDesign(BaseObject):
    yaml_tag = u"!sd"

    def __init__(self, *args, **kwargs):
        super(SolutionDesign, self).__init__(self, *args, **kwargs)
        self.ft = self.get_node_param("ft", "")
        self.desc = self.get_node_param("desc", "")
        self.url = self.get_node_param("url", "")
        self.features = self.get_node_param("features", "")
        self.version = self.get_node_param("version", "")
        self.reg()

    @staticmethod
    def get(name):
        return BaseObject.get_object(SolutionDesign.yaml_tag, name)

    @staticmethod
    def itr():
        return BaseObject.itr(SolutionDesign.yaml_tag)

    def name_url(self):
        return '<a href=%s>%s</a>' % (self.url, self.name)

    def sd_data(self):
        dict_sd_data = ({"name": self.name_url(),
                         "ft": self.ft,
                         "desc": self.desc,
                         "features": self.features})
        return dict_sd_data
