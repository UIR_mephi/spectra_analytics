# -*- coding: utf-8 -*-
import logging

logger = logging.getLogger('env')


def Singleton(cls):
    instances = {}

    def getinstance():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]
    return getinstance


@Singleton
class Env:

    def __init__(self):
        self.classes = {}
        self.objects = {}

    def reg_class(self, cls):
        self.classes[cls.yaml_tag] = cls

    def registered_classes(self):
        for cls in self.classes.values():
            yield cls

    def get_class_by_tag(self, tag):
        return self.classes[tag]

    def reg_object(self, obj_type, obj):
        if obj_type not in self.objects:
            self.objects[obj_type] = {}
        object_dict = self.objects[obj_type]
        if obj.name in object_dict:
            raise Exception("name %s used in %s " % (obj.name, object_dict[obj.name].info))
        object_dict[obj.name] = obj

    def get_object(self, obj_type, name):
        if obj_type not in self.objects:
            raise Exception("Object not defined %s" % obj_type)
        object_dict = self.objects[obj_type]
        if name not in object_dict:
            raise Exception("name %s not found in %s" % (name, obj_type))
        return object_dict[name]

    def object_itr(self, obj_type):
        if obj_type not in self.objects:
            raise Exception("Object not defined %s" % obj_type)
        object_dict = self.objects[obj_type]
        for obj in object_dict.values():
            yield obj
