# -*- coding: utf-8 -*-
import logging
import io
from obj_common import *

logger = logging.getLogger('dev_grp')


@register
class DevGroup(BaseObject):
    yaml_tag = u"!dev_group"

    def __init__(self, *args, **kwargs):
        super(DevGroup, self).__init__(self, *args, **kwargs)
        self.desc = self.get_node_param("desc", "")
        self.home_page = self.get_node_param("home_page", "")
        self.jenkins_space = self.get_node_param("jenkins_space", "")
        self.gitlab_space = self.get_node_param("gitlab_space", "")
        self.publish_page = self.get_node_param("publish_page", "")
        self.reg()

    @staticmethod
    def get(name):
        return BaseObject.get_object(DevGroup.yaml_tag, name)

    @staticmethod
    def itr():
        return BaseObject.itr(DevGroup.yaml_tag)

    def to_html(self):

        pass
