# -*- coding: utf-8 -*-
import logging
import os
import codecs
from obj_common import *

logger = logging.getLogger('incl')

@register
class YamlInclude(BaseObject):
    yaml_tag = u"!include"

    def __init__(self, *args, **kwargs):
        kwargs['node']['name'] = "include_" + str(id(self))
        super(YamlInclude, self).__init__(self, *args, **kwargs)
        self.files = self.get_node_param_ex("files")
        for file in self.files:
            self.process_file_include(file)

    def process_file_include(self, file):
        file_path = os.path.join(self.root_path, file)
        for enc in ['utf-8', 'cp1251']:
            try:
                logger.info("Process file {0}".format(file_path))
                with codecs.open(file_path, encoding=enc) as f:
                    self.loader(f)
            except UnicodeDecodeError:
                pass
            break

    @staticmethod
    def get(name):
        return BaseObject.get_object(YamlInclude.yaml_tag, name)

    @staticmethod
    def itr():
        return BaseObject.itr(YamlInclude.yaml_tag)
