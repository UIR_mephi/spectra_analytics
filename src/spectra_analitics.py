from yaml_loader import *
from env import *
from interface_release import *
from module import *
from dev_group import *
from include import *
from optparse import OptionParser
import json
from json2html import *
import os
from releases import *


def run_spectra_analytics(log_path, yaml_filename, out_dirname, html_filename):
    logging.basicConfig(filename=log_path, filemode='w', level=logging.INFO)
    logger = logging.getLogger('mdl')

    with open(yaml_filename) as f:
        yaml.load(f, SpectraAnalyticsLoader)
    env = Env()

    path_to_file = os.path.dirname(options.html_filename)       #путь к файлу
    if os.path.exists(path_to_file):        # если пользователь указал путь к html-файлу -> записываем в этот файл таблицу
        logger.info("we'l make html file")
        html = open(html_filename, 'w')
        report_html(html)
    else:
        logger.error("path %s to html didn't find" % path_to_file)


def report_ifc():       # функция, создающая отчет по интерфейсам
    data_ifc = []
    for ifc in InterfaceRelease.itr():
        data_ifc.append(ifc.data())
    return json2html.convert(json=data_ifc, table_attributes='width=100% border="1"><col span="7" width=14%', escape=False)


def report_mod():       # функция, создающая отчет по модулям
    data_mod = []
    for mod in Module.itr():
        data_mod.append(mod.data())
    return json2html.convert(json=data_mod, table_attributes='width=100% border="1"><col span="8" width=12.5%', escape=False)       #escape - не будет экранирования - нужно для создания ссылок


def report_release():       # функция, создающая отчет по релизам
    data_rls = []
    for rls in Release.itr():
        data_rls.append(rls.data())
    return json2html.convert(json=data_rls, table_attributes='width=100% border="1"><col span="4" width=25%', escape=False)


def report_html(html):      # функция, создающая html файл со всеми отчетами
    html.write("""
    <HTML>
        <body>
            <h1>Архитектурно-аналитическая система</h1>
            <h2>Описание интерфейсов системы</h2>""")
    html.write(report_ifc())
    html.write("""
            <h2>Описание модулей системы</h2>""")
    html.write(report_mod())
    html.write("""
            <h2>Документирование изменений системы</h2>""")
    html.write(report_release())
    html.write("""
            </body>
    </HTML>""")


if __name__ == '__main__':

    parser = OptionParser()
    parser.add_option("-i", "--input", dest="yaml_filename", default='', help="path to yaml file")
    parser.add_option("-f", "--file", dest="html_filename", default='', help="html-file")
    parser.add_option("-l", "--logging", dest="log_path", default='dds2.log',
                  help="Path to log file")
    parser.add_option("-o", "--output", dest="out_dirname", default='./generated_ini',
                  help="Directory for output files")

    (options, args) = parser.parse_args()

    run_spectra_analytics(options.log_path, options.yaml_filename, options.out_dirname, options.html_filename)

