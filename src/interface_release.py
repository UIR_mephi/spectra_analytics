# -*- coding: utf-8 -*-
import logging
from obj_common import *
from module import *

logger = logging.getLogger('ifc_release')


@register
class InterfaceRelease(BaseObject):
    yaml_tag = u"!interface_release"

    def __init__(self, *args, **kwargs):
        super(InterfaceRelease, self).__init__(self, *args, **kwargs)
        self.desc = self.get_node_param("desc", "")
        self.scheme = self.get_node_param("scheme", "")
        self.version = self.get_node_param("version", "")
        self.interfaces_use = self.get_node_param("interfaces_use", "")
        self.dev_group = self.get_node_param("dev_group", "")
        self.reg()

    @staticmethod
    def get(name):
        return BaseObject.get(InterfaceRelease.yaml_tag, name)

    @staticmethod
    def itr():
        return BaseObject.itr(InterfaceRelease.yaml_tag)

    def get_release_modules(self):
        modules_release_ifc = []
        for mod in Module.itr():
            for mod_ifc in mod.interfaces_release:
                if mod_ifc.name == self.name:
                    modules_release_ifc.append(mod.name)
        return modules_release_ifc

    def name_url(self, list_modules):
        ans = []
        for mod in list_modules:
            url = Module.get(mod).url
            ans.append('<a href=%s>%s</a>' % (url, mod))
        return ans

    def get_use_modules(self):
        modules_use_ifc = []
        for mod in Module.itr():
            for mod_ifc in mod.interfaces_use:
                if mod_ifc.name == self.name:
                    modules_use_ifc.append(mod.name)
        return modules_use_ifc

    def data(self):
        # один блок-строка описания конкретного интерфейса
        data_ifc = ({"Имя интерфейса": self.name,
                     "Описание интерфейса": [self.desc, self.scheme],
                     "Группа разработки": self.dev_group,
                     "Версия": self.version,
                     "Ссылка на hld последнего изменения": "no data",
                     "Модули, использующие этот интерфейс": self.name_url(self.get_use_modules()),
                     "Модуль, реализующий этот интерфейс": self.name_url(self.get_release_modules())})
        return data_ifc
