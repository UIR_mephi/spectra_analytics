import logging
from obj_common import *
from module import *

logger = logging.getLogger('ifc_use')


@register
class InterfaceUse(BaseObject):
    yaml_tag = u"!interface_use"

    def __init__(self, *args, **kwargs):
        super(InterfaceUse, self).__init__(self, *args, **kwargs)
        self.desc = self.get_node_param("desc", "")
        self.scheme = self.get_node_param("scheme", "")
        self.version = self.get_node_param("version", "")
        self.last_version = self.get_node_param("last_version", "")
        self.interfaces_use = self.get_node_param("interfaces_use", "")
        self.dev_group = self.get_node_param("dev_group", "")


    @staticmethod
    def get(name):
        return BaseObject.get(InterfaceUse.yaml_tag, name)

    @staticmethod
    def itr():
        return BaseObject.itr(InterfaceUse.yaml_tag)
