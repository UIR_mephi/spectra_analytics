# -*- coding: utf-8 -*-
import logging
from env import *

logger = logging.getLogger('cmn')

def register(cls):
    Env().reg_class(cls)
    return cls

class YamlObjectInfo():
    def __init__(self, start_mark):
        self.line = start_mark.line + 1
        self.file = start_mark.name

    def __str__(self):
        return "%s:%s" % (self.file, self.line)

    def __repr__(self):
        return self.__str__()


class BaseObject:

    def __init__(self, *args, **kwargs):
        self.node = kwargs['node'] if 'node' in kwargs else {}
        self.info = kwargs['info'] if 'info' in kwargs else None
        self.loader = kwargs['loader'] if 'loader' in kwargs else None
        self.root_path = kwargs['root_path'] if 'root_path' in kwargs else ""
        self.env = Env()
        if 'name' not in self.node:
            err_txt = "Object hasn't name in: %s." % self.info if self.info else "unknown"
            logger.error(err_txt)
            raise Exception(err_txt)
        self.name = self.node['name']

    def get_node_param(self, param_name, param_default):
        return self.node[param_name] if param_name in self.node else param_default

    def get_node_param_ex(self, param_name):
        if param_name not in self.node:
            err_txt = "Object hasn't %s in: %s." % (param_name , self.info if self.info else "unknown")
            logger.error(err_txt)
            raise Exception(err_txt)
        return self.node[param_name]

    def reg(obj):
        env = Env()
        env.reg_object(obj.yaml_tag, obj)

    def get(yaml_tag, name):
        env = Env()
        return env.get_object(yaml_tag, name)

    def itr(yaml_tag):
        env = Env()
        return env.object_itr(yaml_tag)

    def __str__(self):
        return "%s: %s" % (self.yaml_tag, self.name)

    def __repr__(self):
        return self.__str__()
