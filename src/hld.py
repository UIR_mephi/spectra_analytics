# -*- coding: utf-8 -*-
import logging
from obj_common import *

logger = logging.getLogger('hld')


@register
class HiLevelDesign(BaseObject):
    yaml_tag = u"!hld"

    def __init__(self, *args, **kwargs):
        super(HiLevelDesign, self).__init__(self, *args, **kwargs)
        self.desc = self.get_node_param("desc", "")
        self.version = self.get_node_param("version", "")
        self.url = self.get_node_param("url", "")
        self.reg()

    @staticmethod
    def get(name):
        return BaseObject.get(HiLevelDesign.yaml_tag, name)

    @staticmethod
    def itr():
        return BaseObject.itr(HiLevelDesign.yaml_tag)
