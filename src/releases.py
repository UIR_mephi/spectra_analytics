# -*- coding: utf-8 -*-
import logging
from obj_common import *

logger = logging.getLogger('releases')


@register
class Release(BaseObject):
    yaml_tag = u"!spectra_release"

    def __init__(self, *args, **kwargs):
        super(Release, self).__init__(self, *args, **kwargs)
        self.url = self.get_node_param("url", "")
        self.sds = self.get_node_param("sds", "")
        self.reg()

    @staticmethod
    def get(name):
        return BaseObject.get_object(Release.yaml_tag, name)

    @staticmethod
    def itr():
        return BaseObject.itr(Release.yaml_tag)

    def solution_designs(self):
        list_sds = []
        for sd in self.sds:
            list_sds.append(sd.sd_data())
        return list_sds

    def name_url(self):
        return '<a href=%s>%s</a>' % (self.url, self.name)

    def data(self):
        data_rls = ({"Имя релиза": self.name_url(),
                     "Solution design": self.solution_designs()})
        return data_rls
