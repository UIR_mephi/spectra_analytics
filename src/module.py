# -*- coding: utf-8 -*-
import logging
from obj_common import *
from interface_release import *
from interface_use import *
from env import *
from hld import *
logger = logging.getLogger('mdl')


@register
class Module(BaseObject):
    yaml_tag = u"!module"

    def __init__(self, *args, **kwargs):
        super(Module, self).__init__(self, *args, **kwargs)
        self.desc = self.get_node_param("desc", "")
        self.scheme = self.get_node_param("scheme", "")
        self.url = self.get_node_param("url", "")
        self.version = self.get_node_param("version", "")
        self.dev_group = self.get_node_param("dev_group", "")
        self.hlds = self.get_node_param("hlds", [])
        self.interfaces_release = self.get_node_param("interfaces_release", [])
        self.interfaces_use = self.get_node_param("interfaces_use", [])
        self.reg()

    @staticmethod
    def get(name):
        return BaseObject.get(Module.yaml_tag, name)

    @staticmethod
    def itr():
        return BaseObject.itr(Module.yaml_tag)

    def mod_connected_next(self):
        connected_modules = []
        for ifc in self.interfaces_release:
            for mod in ifc.get_use_modules():
                connected_modules.append(mod)
        return sorted(connected_modules)

    def mod_connected_before(self):
        connected_modules = []
        for ifc in self.interfaces_use:
            for mod in Module.itr():
                for mod_ifc in mod.interfaces_release:
                    if ifc.name == mod_ifc.name:
                        connected_modules.append(mod.name)
        return sorted(connected_modules)

    def mod_connected(self):
        return list(set(self.mod_connected_before()+self.mod_connected_next()))

    def mod_hld(self):
        dict_mod_hld = {}
        for hld in HiLevelDesign.itr():
            for h in self.hlds:
                if h == hld:
                    dict_mod_hld[hld.name] = [hld.version, hld.url]
        return dict_mod_hld

    def name_url(self):
        return '<a href=%s>%s</a>' % (self.url, self.name)

    def data(self):
        # один блок-строка описания конкретного модуля
        data_mod = ({"Имя модуля": self.name_url(),
                     "Описание модуля": self.desc,
                     "Группа разработки": self.dev_group,
                     "Версия": self.version,
                     "Ссылка на hld последнего изменения": self.mod_hld(),
                     "Модули, предоставляющие интерфейсы": self.mod_connected_before(),
                     "Модули, ипользующие интерфейсы": self.mod_connected_next(),
                     "Модули, взаимодействующие с этим модулем": self.mod_connected()})
        return data_mod
